import logo from './logo.svg';
import './App.css';
import { Customer } from './stories/customer/Customer';

function App() {
  return (
    <div className="App">
     <Customer/>
    </div>
  );
}

export default App;
