import React from "react"
import {Panel, Button} from 'react-bootstrap'

export const Customer = ({customer = customer, selectCustomer= ()=>{}}) => (<Panel bsStyle="info" key={customer.name} className="centeralign">
    <Panel.Heading>
        <Panel.Title componentClass="h3">{customer.name}</Panel.Title>
    </Panel.Heading>
    <Panel.Body>
        <p>{customer.email}</p>
        <p>{customer.phone}</p>
        <Button bsStyle="info" onClick={() => selectCustomer(customer.id)}>

            Click to View Details

        </Button>

    </Panel.Body>
</Panel>)

const customer = {
      id: '1',
      name: 'Tapas',
      email: 'xxxxx@XXXX',
      phone: '+xxxxxxxxx'
  };