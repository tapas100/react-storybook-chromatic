import React from 'react';

import { Customer } from './Customer';

export default {
  component: Customer,
  title: 'Customer',
};

const Template = args => <Customer {...args} />;

export const Default = Template.bind({});
Default.args = {
  customer: {
    id: '1',
    name: 'Tapas',
    email: 'xxxxx@XXXX',
    phone: '+xxxxxxxxx'
  },
};

export const Pinned = Template.bind({});
Pinned.args = {
  customer: {
    ...Default.args.customer,
  },
};

export const Archived = Template.bind({});
Archived.args = {
  customer: {
    ...Default.args.customer,
  },
};